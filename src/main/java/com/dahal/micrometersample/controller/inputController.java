package com.dahal.micrometersample.controller;

import io.micrometer.core.annotation.Timed;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class inputController {

    @RequestMapping("/hello")
    public String getHello(){
        return "Hello";
    }
}
