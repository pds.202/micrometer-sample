package com.dahal.micrometersample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//@ComponentScan("com.dahal.micrometersample.controller")
@SpringBootApplication
public class MicrometerSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicrometerSampleApplication.class, args);
    }

}
